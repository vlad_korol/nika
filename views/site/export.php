<?php

/* @var $this yii\web\View */

use app\components\WPdumpParser;
use kartik\select2\Select2;
use kartik\sortinput\SortableInput;
use yii\bootstrap\BaseHtml;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Export WP dumps';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-export">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the Converter page. Convert WP dump to csv:
    </p>

    <?php
    echo BaseHtml::beginForm("export", "post", [
        "name" => WPdumpParser::FORM_SORTABLE_NAME
    ]);

    echo '<div class="row">';
    echo '<div class="col-sm-6">';
    echo '<h4>Files for parse</h4>';
    echo SortableInput::widget([
        'name' => 'export',
        'items' => WPdumpParser::getItems(),
        'hideInput' => false,
        'sortableOptions' => [
            'connected' => true,
        ],
        'options' => ['class' => 'form-control', 'readonly' => true]
    ]);
    echo '</div>';
    echo '<div class="col-sm-6">';
    echo '<h4>Files for delete</h4>';
    echo SortableInput::widget([
        'name' => 'delete',
        'items' => [],
        'hideInput' => false,
        'sortableOptions' => [
            'itemOptions' => ['class' => 'alert alert-warning'],
            'connected' => true,
        ],
        'options' => ['class' => 'form-control', 'readonly' => true]
    ]);
    echo '</div>';
    echo '</div>';  // end row
    echo '<div class="row">';


    echo '<hr>';
    echo '<div class="col-sm-6">';
    echo Select2::widget([
        'name' => 'exportFormat',
        'hideSearch' => true,
        'size' => 'sm',
        'theme' => Select2::THEME_BOOTSTRAP,
        'value' => 'csv', // initial value
        'data' => [
            WPdumpParser::OUTPUT_TYPE_CSV => WPdumpParser::OUTPUT_TYPE_CSV,
            WPdumpParser::OUTPUT_TYPE_TXT => WPdumpParser::OUTPUT_TYPE_TXT,
            WPdumpParser::OUTPUT_TYPE_XML => WPdumpParser::OUTPUT_TYPE_XML],
        'options' => ['multiple' => false, 'placeholder' => 'Select output format ...'],
        "pluginEvents" => [
            "select2:select" => "function(e) { 
            console.log(e);
               if(e.target.value == '" . WPdumpParser::OUTPUT_TYPE_XML . "') {
                    $('#delimiter').hide();
               } else{
                    $('#delimiter').show();
               }
               
             }",
        ]
    ]);

    echo '</div>';

    echo '<div class="col-sm-6">';
    echo Html::textInput("delimiter", "", [
        "id" => "delimiter",
        "class" => "form-control",
        "placeholder" => 'Set delimiter here...'
    ]);
    echo '</div>';
    echo '<div class="col-sm-12"><h5>You can Drag and Drop file names for sort or delete files</h5></div>';
    echo '</div>'; // end row
    echo '<br>';
    echo Html::submitButton('Submit Form', [
        'class' => 'btn btn-default',
        'id' => 'submit-upload'
    ]);

    echo BaseHtml::endForm();
    ?>

    <hr>

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'files[]')->fileInput(['multiple' => true]) ?>

    <button>Submit</button>

    <?php ActiveForm::end() ?>
</div>
