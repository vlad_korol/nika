<?php
/**
 * Created by IntelliJ IDEA.
 * User: Vlad
 * Date: 09.07.2017
 * Time: 12:35
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile
     */
    public $files;

    public function rules()
    {
        return [
            [['files'], 'file',
                'skipOnEmpty' => false,
                'extensions' => ['sql'],
                'checkExtensionByMimeType' => false,
                'maxFiles' => 100,
                'maxSize' => 5 * 1024 * 1024
            ],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            foreach ($this->files as $file) {
                $file->saveAs('export/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }
}