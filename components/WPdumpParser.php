<?php
/**
 * Created by IntelliJ IDEA.
 * User: Vlad
 * Date: 10.07.2017
 * Time: 21:45
 */

namespace app\components;


use PHPSQLParser\PHPSQLParser;
use SimpleXMLElement;
use Yii;
use yii\helpers\FileHelper;

class WPdumpParser
{

    const CSV_FILE_NAME = "result";
    const FORM_SORTABLE_NAME = "CreateCsv";
    const OUTPUT_TYPE_CSV = "csv";
    const OUTPUT_TYPE_TXT = "txt";
    const OUTPUT_TYPE_XML = "xml";

    /**
     * This is main function for create csv file
     * @param string $filesName
     * @param string $outputType
     * @param string $delimiter
     */
    public static function init($filesName = "", $outputType = "csv", $delimiter = ";")
    {
        foreach (explode(",", $filesName) as $fileName) {
            $file = Yii::getAlias("@webroot") . "/export/" . $fileName;
            if (file_exists($file) && filesize($file) != 0) {
                $contents[] = self::getInsertString($file);
            }
        }

        switch ($outputType) {
            case self::OUTPUT_TYPE_CSV :
                self::generateCsv($contents ?? [], $delimiter);
                break;
            case self::OUTPUT_TYPE_TXT :
                self::generateTxt($contents ?? [], $delimiter);
                break;
            case self::OUTPUT_TYPE_XML :
                self::generateXml($contents ?? []);
                break;
        }
    }

    /**
     * This function get INSERT query from dump
     * @param $file
     * @return array
     */
    public static function getInsertString($file)
    {

        $posts = [];

        foreach (explode("\n\n", file_get_contents($file)) as $sql) {
            if (strpos($sql, "INSERT") !== false && strpos($sql, "post_content") !== false) {
                $posts = array_merge($posts, self::parseSql($sql));
            }
        }
        return $posts;
    }

    /**
     * This function remove some html tags from content
     * @param string $string
     * @return mixed|string
     */
    public static function prepareString($string = "")
    {
        $result = substr($string, 1, -1);
        $result = preg_replace("/<img[^>]+\>/i", " ", $result);
        $result = preg_replace('/<a[^>]+\>|<\/a>/s', '', $result);

        return $result;
    }

    /**
     * This function create array for csv file
     * @param array $sqlArray
     * @return array
     */
    public static function getPostDataFromArray(array $sqlArray = [])
    {
        foreach ($sqlArray["VALUES"] as $value) {

            // Skipping if empty content or post not publish
            // if You need only published posts add `strpos($value["data"][7]['base_expr'], "publish") === false` to IF
            if (empty($value["data"][4]['base_expr']) || strlen($value["data"][4]['base_expr']) < 4) continue;

            $result[] = [
                'title' => self::prepareString($value["data"][5]['base_expr'] ?? ""),
                'content' => self::prepareString($value["data"][4]['base_expr']),
                'state' => substr($value["data"][7]['base_expr'] ?? "", 1, -1),
                'date' => substr($value["data"][2]['base_expr'] ?? "", 1, -1),
            ];
        }

        return $result ?? [];
    }

    /**
     * This function parse WP sql dump
     * @param string $sql
     * @return array
     */
    public static function parseSql($sql = "")
    {

        $parser = new PHPSQLParser($sql);

        return self::getPostDataFromArray($parser->parsed);

    }

    /**
     * This function delete files
     * @param $filesName
     */
    public static function unlinkFiles($filesName)
    {
        foreach (FileHelper::findFiles('export/', ['only' => explode(",", $filesName)]) as $file) {
            unlink(Yii::getAlias("@webroot") . DIRECTORY_SEPARATOR . $file);
        }
    }

    /**
     * This function for getting all sql files from upload directory
     * @return array
     */
    public static function getItems()
    {
        $files = FileHelper::findFiles('export/', ['only' => ['*.sql']]);
        $items = [];
        if (isset($files[0])) {
            foreach ($files as $index => $file) {
                $fileName = substr($file, strrpos($file, '/') + 1);
                $items[$fileName] = ['content' => $fileName];
            }
        } else {
            echo "There are no files available for download.";
        }
        return $items;
    }

    /**
     * This function create csv file
     * @param array $data
     * @param $delimiter
     */
    public static function generateCsv(array $data = [], $delimiter)
    {

        $file = Yii::getAlias("@webroot") . "/export/" . self::CSV_FILE_NAME . "." . self::OUTPUT_TYPE_CSV;

        if (file_exists($file)) unlink($file);

        $fp = fopen($file, 'w');

        $delimiter = !empty($delimiter) ? substr($delimiter, 0, 1) : ";";

        foreach ($data as $part) {
            foreach ($part as $row)
                fputcsv($fp, $row, $delimiter);
        }

        fclose($fp);

        Yii::$app->getResponse()->sendFile($file);

    }

    /**
     * This function create csv file
     * @param array $data
     */
    public static function generateXml(array $data = [])
    {

        $file = Yii::getAlias("@webroot") . "/export/" . self::CSV_FILE_NAME . "." . self::OUTPUT_TYPE_XML;

        if (file_exists($file)) unlink($file);

        $xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><posts/>');

        foreach ($data as $part) {
            foreach ($part as $row) {
                $post = $xml->addChild('post');
                $post->addChild('title', $row['title']);
                $post->addChild('content', $row['content']);
                $post->addChild('state', $row['state']);
                $post->addChild('date', $row['date']);
            }
        }

        $file = \Yii::getAlias('@webroot') . '/export/' . self::CSV_FILE_NAME . '.' . self::OUTPUT_TYPE_XML;

        file_put_contents($file, $xml->asXML());

        Yii::$app->getResponse()->sendFile($file);

    }

    /**
     * This function create csv file
     * @param array $data
     * @param $delimiter
     */
    public static function generateTxt(array $data = [], $delimiter)
    {

        $file = Yii::getAlias("@webroot") . "/export/" . self::CSV_FILE_NAME . "." . self::OUTPUT_TYPE_TXT;

        if (file_exists($file)) unlink($file);

        $text = '';
        foreach ($data as $part) {
            foreach ($part as $row) {
                $text .= $row['title'] . $delimiter . $row['content'] . $delimiter . $row['state'] . $delimiter . $row['date'] . PHP_EOL;
            }
        }

        file_put_contents($file, $text);

        Yii::$app->getResponse()->sendFile($file);

    }

}